package co.com.pragma.ejerciciotransacciones.dao;

import co.com.pragma.ejerciciotransacciones.domain.Persona;
import org.springframework.data.repository.CrudRepository;

public interface IPersona extends CrudRepository<Persona,Long> {}
