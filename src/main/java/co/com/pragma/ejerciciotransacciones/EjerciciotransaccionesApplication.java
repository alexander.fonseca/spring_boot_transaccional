package co.com.pragma.ejerciciotransacciones;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EjerciciotransaccionesApplication {

	public static void main(String[] args) {
		SpringApplication.run(EjerciciotransaccionesApplication.class, args);
	}

}
