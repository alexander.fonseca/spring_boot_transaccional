package co.com.pragma.ejerciciotransacciones.domain;

import lombok.Data;
import org.hibernate.validator.constraints.NotEmpty;

import javax.persistence.*;
import javax.validation.constraints.Email;


@Data
@Entity
@Table(name = "persona")
public class Persona {

    private static final long serialVersionUID=1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long idPersona;
    @NotEmpty
    private String nombre;
    @NotEmpty
    private String apellido;

    private String telefono;

    @NotEmpty
    @Email
    private String email;

}
